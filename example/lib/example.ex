defmodule Example do
  def factorial(0), do: 1
  def factorial(n) when n > 0, do: n* factorial(n-1)
  def suma([], acc), do: acc
  def suma([head | tail], acc), do: suma(tail, acc + head)
  def len([]), do: 0
  def len([_head | tail]), do: 1 + len(tail)
end
